from django.core.management.base import BaseCommand
import sys
import logging
from minechangelogs import models as mmodels

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Update minechangelogs index'

    def add_arguments(self, parser):
        parser.add_argument("--quiet", action="store_true", dest="quiet", default=None,
                            help="Disable progress reporting")
        parser.add_argument("--oldentries", action="store", metavar="FILE",
                            help="Also read old entries from a file")

    def handle(self, oldentries=None, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if opts["quiet"]:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        indexer = mmodels.Indexer()
        if oldentries:
            indexer.index(mmodels.parse_changelog(oldentries))

        with mmodels.parse_projectb() as changes:
            indexer.index(changes)
        indexer.flush()
