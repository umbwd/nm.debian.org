from django.utils.timezone import now
from . import models as pmodels
from . import ops as pops


def ping_stuck_processes(stuck_cutoff, audit_author, logdate=None):
    from .email import ping_process
    if logdate is None:
        logdate = now()

    for process in pmodels.Process.objects.in_early_stage():
        already_pinged = False
        for idx, entry in enumerate(process.log.order_by("logdate")):
            if entry.action == "ping":
                already_pinged = True
        if entry.logdate > stuck_cutoff:
            continue

        # get missing requirements for process in order
        # to be able to issue a more meaningful ping message

        missing_requirements = []
        # additional information appended to ping mail -
        # keycheck does not count as missing requirement here, but is often misinterpreted,
        # so append a message, if it is not approved yet.
        keycheck_message = ""
        # an advocate might take longer than one week to react
        # and the applicant might not be able to influence that,
        # so append a message, that a closed process can be reopened.
        advocate_message = ""

        for r in process.requirements.all():
            if r.type == "advocate" or r.type == "intent" or r.type == "sc_dmup":
                if r.approved_by is None:
                    missing_requirements.append(r)
            # add additional messages for missing advocate and keycheck
            if r.type == "keycheck" and r.approved_by is None:
                keycheck_message = f"""-> Don't worry about {r} -
   it's not taken into regard here, because it needs manual approval.
"""
            if r.type == "advocate" and r.approved_by is None:
                advocate_message = """\
-> If you have an advocate that just needs some more time to react,
   the process can be reopened.
"""
        # not having any missing requirements here would be an error,
        # but let's don't have it look weird in the mail and omit the additional text
        if len(missing_requirements) <= 0:
            msg_str = ""
        else:
            # differentiating singular/plural
            if len(missing_requirements) < 2:
                req = "requirement isn't fulfilled"
            else:
                req = "requirements aren't fulfilled"
            msg_str = f"""
This message was triggered because the following {req}:
"""
            for r in missing_requirements:
                msg_str = f"{msg_str}- {r}\n"

        if not already_pinged:
            # Detect first instance of processes stuck early: X days from
            # last log, no previous ping message
            msg_str = f"""{msg_str}
If nothing happens, the process will be automatically closed a week from now.
{keycheck_message}{advocate_message}"""

            ping_process(audit_author, process, message=msg_str)
            process.add_log(audit_author, "looks stuck, pinged",
                            action="ping", logdate=logdate)
        else:
            # Detect second instance: X days from last log, no
            # intent/advocate/sc_dmup, a previous ping message
            msg_str = f"""{msg_str}
A week has passed from the last ping with no action, I'll now close the
process. Feel free to reapply in the future.
{advocate_message}
"""
            ping_process(audit_author, process, message=msg_str)
            process.add_log(audit_author, "closing for inactivity",
                            action="proc_close", logdate=logdate, is_public=True)
            process.closed_by = audit_author
            process.closed_time = logdate
            process.save()


def submit_rt_ticket_for_fd_approved_processes(approval_to_rt_delay):
    """
    Create a RT ticket for all processes approved by a FD member, older than
    approval_to_rt_delay, without a RT ticket already
    """
    for process in pmodels.Process.objects.approved_without_RT():
        # The processes here have an approval requirement per the request done
        # to fetch them. Hence, get will return.
        appr = process.requirements.get(type="approval")
        for statement in appr.statements.all():
            am = statement.fpr.person.am_or_none
            if am is None:
                continue

            if am.is_dam:
                send_rt_ticket(process, statement)
            elif am.is_fd and now() - appr.approved_time > approval_to_rt_delay:
                send_rt_ticket(process, statement)


def send_rt_ticket(process, statement):
    """
    Actually sends a rt ticketa for a process
    """

    op = pops.ProcessRT(
        process=process,
        audit_author=statement.fpr.person,
        audit_notes="Opened RT ticket",
    )
    op.rt_text = statement.statement
    op.execute()
