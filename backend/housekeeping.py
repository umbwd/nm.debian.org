from django.utils.timezone import now
from django.conf import settings
import django_housekeeping as hk
from django.db import transaction
from django.contrib.sites.shortcuts import get_current_site
from . import const, utils
from . import models as bmodels
import legacy.models as lmodels
from sitechecks.models import Inconsistency
from signon.models import Identity
import gzip
import datetime
import json
import os.path
import logging

log = logging.getLogger(__name__)

BACKUP_DIR = getattr(settings, "BACKUP_DIR", None)

STAGES = ["backup", "main", "stats"]


class Housekeeper(hk.Task):
    NAME = "housekeeper"

    def __init__(self, *args, **kw):
        super(Housekeeper, self).__init__(*args, **kw)
        # Ensure that there is a __housekeeping__ user
        self.user = bmodels.Person.objects.get_housekeeper()


class MakeLink(hk.Task):
    NAME = "link"

    def __init__(self, *args, **kw):
        super(MakeLink, self).__init__(*args, **kw)
        self.site = get_current_site(None)

    def __call__(self, obj):
        if self.site.domain == "localhost":
            return "http://localhost:8000" + obj.get_absolute_url()
        else:
            return "https://%s%s" % (self.site.domain, obj.get_absolute_url())


class BackupDB(hk.Task):
    """
    Backup of the whole database
    """
    def run_backup(self, stage):
        from backend.export import export_db

        if self.hk.outdir is None:
            log.info("HOUSEKEEPING_ROOT is not set: skipping backups")
            return

        exported = export_db(full=True)

        class Serializer(json.JSONEncoder):
            def default(self, o):
                if hasattr(o, "strftime"):
                    return o.strftime("%Y-%m-%d %H:%M:%S")
                return json.JSONEncoder.default(self, o)

        # Base filename for the backup
        basedir = self.hk.outdir.path()
        fname = os.path.join(basedir, "db-full.json.gz")
        log.info("%s: backing up to %s", self.IDENTIFIER, fname)
        if self.hk.dry_run:
            return

        # Write the backup file
        with utils.atomic_writer(fname, mode="wb", chmod=0o640) as fd:
            with gzip.open(fd, mode="wt", compresslevel=9) as gzfd:
                json.dump(exported, gzfd, cls=Serializer, indent=2)


class ComputeActiveAM(hk.Task):
    """
    Compute AM Committee membership
    """
    DEPENDS = [MakeLink]

    def _processes_make_active(self, am_assignments):
        threshold = now() - datetime.timedelta(days=365)
        for a in am_assignments:
            if not a.process.closed:
                return True
            if a.process.closed_time > threshold:
                return True
        return False

    @transaction.atomic
    def run_main(self, stage):
        import process.models as pmodels

        new_inactive = 0

        for am in bmodels.AM.objects.filter(is_am=True).select_related("person"):
            if not am.person.is_dd:
                log.info("%s: %s is not a DD anymore, removing active AM flag",
                         self.IDENTIFIER, self.hk.link(am.person))
                am.is_am = False
                am.save()
                new_inactive += 1
                continue

            if am.slots == 0:
                if self._processes_make_active(pmodels.AMAssignment.objects.filter(am=am).select_related("process")):
                    continue
                log.info("%s: %s has been inactive too long, removing active AM flag",
                         self.IDENTIFIER, self.hk.link(am.person))
                am.is_am = False
                am.save()
                new_inactive += 1
                continue

        log.info("%s: %d AMs marked inactive", self.IDENTIFIER, new_inactive)


class ComputeAMCTTE(hk.Task):
    """
    Compute AM Committee membership
    """
    DEPENDS = [ComputeActiveAM]

    @transaction.atomic
    def run_main(self, stage):
        # Touch $DATA_DIR/convene_ctte to temporarily stop updating the ctte
        # membership while the CTTE is asked to take some decision, to give
        # people the time to work without their membership fluctuating
        if os.path.exists(os.path.join(settings.DATA_DIR, "convene_ctte")):
            log.warn("%s: convene_ctte file present: skipping updating CTTE membership", self.IDENTIFIER)
            return

        import process.models as pmodels

        # Set all to False
        bmodels.AM.objects.update(is_am_ctte=False)

        cutoff = now()
        cutoff = cutoff - datetime.timedelta(days=30 * 6)

        # Add the AMs who had a process approved in the last 6 months
        ctte = set(x.am for x in pmodels.AMAssignment.objects.filter(
            process__approved_time__gt=cutoff, unassigned_time__isnull=True))
        # Add FD
        ctte.update(bmodels.AM.objects.filter(is_fd=True))
        # Add DAM
        ctte.update(bmodels.AM.objects.filter(is_dam=True))

        for am in ctte:
            am.is_am_ctte = True
            am.save()

        log.info("%s: %d CTTE members", self.IDENTIFIER, bmodels.AM.objects.filter(is_am_ctte=True).count())


class PersonExpires(hk.Task):
    """
    Expire old Person records
    """
    DEPENDS = [MakeLink, Housekeeper]

    @transaction.atomic
    def run_main(self, stage):
        """
        Generate a sequence of Person objects that have expired
        """
        import process.models as pmodels
        today = datetime.date.today()
        for p in bmodels.Person.objects.filter(expires__lt=today):
            if p.status != const.STATUS_DC:
                log.info("%s: removing expiration date for %s who has become %s",
                         self.IDENTIFIER, self.hk.link(p), p.status)
                p.expires = None
                p.save(
                    audit_author=self.hk.housekeeper.user,
                    audit_notes="user became {}: removing expiration date".format(
                        const.ALL_STATUS_DESCS_WITH_PRONOUN[p.status]))
            elif p.legacy_processes.exists() or pmodels.Process.objects.filter(person=p).exists():
                log.info("%s: removing expiration date for %s who now has process history",
                         self.IDENTIFIER, self.hk.link(p))
                p.expires = None
                p.save(audit_author=self.hk.housekeeper.user, audit_notes="process detected: removing expiration date")
            else:
                log.info("%s: deleting expired Person %s", self.IDENTIFIER, p)
                p.delete()


class CheckOneProcessPerPerson(hk.Task):
    """
    Check that one does not have more than one open process at the current time
    """
    def run_main(self, stage):
        from django.db.models import Count
        # TODO: count new-style processes and not old style ones
        for p in bmodels.Person.objects.filter(legacy_processes__is_active=True) \
                .annotate(num_processes=Count("legacy_processes")) \
                .filter(num_processes__gt=1):
            Inconsistency.objects.found(self.IDENTIFIER, "person has more than 1 open processes", person=p)


class CheckAMMustHaveUID(hk.Task):
    """
    Check that AMs have a Debian login
    """
    def run_main(self, stage):
        for am in bmodels.AM.objects.filter(person__ldap_fields__uid=None):
            Inconsistency.objects.found(self.IDENTIFIER, "AM has empty Person.ldap_fields__uid", person=am.person)


class CheckStatusProgressMatch(hk.Task):
    """
    Check that the last process with progress 'done' has the same
    'applying_for' as the person status
    """

    def run_main(self, stage):
        import process.models as pmodels

        process_byperson = {}

        for p in lmodels.Process.objects.filter(
                closed__isnull=False, progress=const.PROGRESS_DONE).select_related("person"):
            existing = process_byperson.get(p.person, None)
            if existing is None:
                process_byperson[p.person] = p
            elif existing.closed < p.closed:
                process_byperson[p.person] = p

        for p in pmodels.Process.objects.filter(
                closed_time__isnull=False, approved_by__isnull=False).select_related("person"):
            existing = process_byperson.get(p.person, None)
            if existing is None:
                process_byperson[p.person] = p
            elif isinstance(existing, lmodels.Process) and existing.closed < p.closed_time:
                process_byperson[p.person] = p
            elif isinstance(existing, pmodels.Process) and existing.closed_time < p.closed_time:
                process_byperson[p.person] = p

        for person, process in list(process_byperson.items()):
            if person.status != process.applying_for:
                if isinstance(process, lmodels.Process):
                    Inconsistency.objects.found(
                        self.IDENTIFIER,
                        "person has status {} but the last completed process was applying for {}".format(
                            person.status, process.applying_for), person=person)
                else:
                    Inconsistency.objects.found(
                        self.IDENTIFIER,
                        "person has status {} but the last completed process was applying for {}".format(
                            person.status, process.applying_for), person=person, process=process)


class CheckEnums(hk.Task):
    """
    Consistency check of enum values
    """

    def run_main(self, stage):
        import process.models as pmodels
        statuses = [x.tag for x in const.ALL_STATUS]

        for p in bmodels.Person.objects.exclude(status__in=statuses):
            Inconsistency.objects.found(self.IDENTIFIER, "invalid status {}".format(p.status), person=p)

        for p in pmodels.Process.objects.exclude(applying_for__in=statuses):
            Inconsistency.objects.found(self.IDENTIFIER, "invalid applying for {}".format(p.status), process=p)


class CheckCornerCases(hk.Task):
    """
    Check for known corner cases, to be fixed somehow eventually maybe in case
    they give trouble
    """
    def run_main(self, stage):
        c = bmodels.Person.objects.filter(legacy_processes__isnull=True).count()
        if c > 0:
            log.info("%s: %d Great Ancients found who have no Process entry", self.IDENTIFIER, c)

        for p in bmodels.Person.objects.filter(status_changed__isnull=True):
            Inconsistency.objects.found(self.IDENTIFIER, "status_changed is empty", person=p)


class CheckDjangoPermissions(hk.Task):
    """
    Check consistency between Django permissions and flags in the AM model
    """
    def run_main(self, stage):
        from django.db.models import Q

        # Get the list of users that django thinks are powerful
        person_power_users = set()
        for p in bmodels.Person.objects.all():
            if p.is_staff or p.is_superuser:
                person_power_users.add(p.id)

        # Get the list of users that we think are powerful
        am_power_users = set()
        for a in bmodels.AM.objects.filter(Q(is_fd=True) | Q(is_dam=True)):
            am_power_users.add(a.person.id)

        for id in (person_power_users - am_power_users):
            p = bmodels.Person.objects.get(pk=id)
            Inconsistency.objects.found(
                    self.IDENTIFIER,
                    "person is staff or superuser, but the AM record does not list them as FD or DAM", person=p)
        for id in (am_power_users - person_power_users):
            p = bmodels.Person.objects.get(pk=id)
            Inconsistency.objects.found(
                    self.IDENTIFIER,
                    "person is FD or DAM in the AM record, but is not listed as staff or superuser", person=p)


class DDUsernames(hk.Task):
    """
    Make sure that people with a DD status have a DD debsso identity
    """
    DEPENDS = [MakeLink, Housekeeper]

    @transaction.atomic
    def run_main(self, stage):
        dd_statuses = (const.STATUS_DD_U, const.STATUS_DD_NU)
        post_dd_statuses = (const.STATUS_EMERITUS_DD, const.STATUS_REMOVED_DD)
        for p in bmodels.Person.objects.filter(status__in=dd_statuses + post_dd_statuses):
            if p.ldap_fields.uid is None:
                Inconsistency.objects.found(
                        self.IDENTIFIER, "person has status {} but uid is empty".format(p.status), person=p)
                continue

            debsso = list(p.identities.filter(issuer="debsso").all())
            if p.status in dd_statuses:
                # The person is an active DD
                canonical_username = "{}@debian.org".format(p.ldap_fields.uid)

                has_canonical = False
                for identity in debsso:
                    if identity.subject.endswith("@debian.org") and identity.subject != canonical_username:
                        Inconsistency.objects.found(
                                self.IDENTIFIER,
                                f"person has debsso debian identity {identity.subject} instead of {canonical_username}",
                                person=p)
                    elif identity.subject == canonical_username:
                        has_canonical = True

                if not has_canonical:
                    log.info("%s: %s has no %s debsso identity: adding it",
                             self.IDENTIFIER, self.hk.link(p), canonical_username)
                    if not self.hk.dry_run:
                        try:
                            identity = Identity.objects.get(
                                issuer="debsso",
                                subject=canonical_username,
                            )
                        except Identity.DoesNotExist:
                            identity = Identity.objects.create(
                                issuer="debsso",
                                subject=canonical_username,
                                username=canonical_username,
                                audit_author=self.hk.housekeeper.user,
                                audit_notes="create SSO identity to match the person's status"
                            )
                        identity.person = p
                        identity.save(
                            audit_author=self.hk.housekeeper.user,
                            audit_notes="bind the identity to the user"
                        )
            elif p.status in post_dd_statuses:
                # The person was an active DD, and is now emeritus or removed
                has_alioth = False
                for identity in debsso:
                    # Remove @debian.org debsso identities
                    if identity.subject.endswith("@debian.org"):
                        log.info("%s: %s removing old @debian.org debsso identity %s",
                                 self.IDENTIFIER, self.hk.link(p), identity.subject)
                        identity.delete()
                    elif identity.subject.endswith("@users.alioth.debian.org"):
                        # Check if an alioth identity already exists
                        has_alioth = True

                if not has_alioth:
                    canonical_username = "{}@users.alioth.debian.org".format(p.ldap_fields.uid)
                    log.info("%s: %s has no %s debsso identity: adding it",
                             self.IDENTIFIER, self.hk.link(p), canonical_username)
                    if not self.hk.dry_run:
                        try:
                            identity = Identity.objects.get(
                                issuer="debsso",
                                subject=canonical_username,
                            )
                        except Identity.DoesNotExist:
                            identity = Identity.objects.create(
                                issuer="debsso",
                                subject=canonical_username,
                                username=canonical_username,
                                audit_author=self.hk.housekeeper.user,
                                audit_notes="create SSO identity to match the person's status"
                            )
                        identity.person = p
                        identity.save(
                            audit_author=self.hk.housekeeper.user,
                            audit_notes="bind the identity to the user"
                        )


class CheckOneActiveKeyPerPerson(hk.Task):
    """
    Check that one does not have more than one open process at the current time
    """
    def run_main(self, stage):
        from django.db.models import Count
        for p in bmodels.Person.objects.filter(fprs__is_active=True) \
                .annotate(num_fprs=Count("fprs")) \
                .filter(num_fprs__gt=1):
            Inconsistency.objects.found(
                    self.IDENTIFIER, "person has {} active gpg keys".format(p.num_fprs), person=p)


class ProcmailIncludes(hk.Task):
    """
    Generate the procmail include files for am and ctte aliases
    """
    DEPENDS = [ComputeActiveAM]

    def run_main(self, stage):
        root = getattr(settings, "MAIL_CONFDIR", None)
        if root is None:
            log.warning("%s: MAIL_CONFDIR is not set: skipping task", self.IDENTIFIER)
            return
        if not os.path.isdir(root):
            log.warning("%s: MAIL_CONFDIR is set to %s which does not exist or is not a directory; skipping task",
                        self.IDENTIFIER, root)
            return

        # Generate AM list
        with utils.atomic_writer(os.path.join(root, "am.include"), mode="wt", chmod=0o600) as fd:
            print("# Automatically generated by NM housekeeping at {} - do not edit".format(now()), file=fd)
            for am in bmodels.AM.objects.all().select_related("person"):
                if not (am.person.is_superuser or am.is_am):
                    continue
                print(":0c", file=fd)
                print("! {}".format(am.person.email), file=fd)

        # Generate NM CTTE list
        with utils.atomic_writer(os.path.join(root, "nm-committee.include"), mode="wt", chmod=0o600) as fd:
            print("# Automatically generated by NM housekeeping at {} - do not edit".format(now()), file=fd)
            for am in bmodels.AM.objects.all().select_related("person"):
                if not (am.person.is_superuser or am.is_am_ctte):
                    continue
                print(":0c", file=fd)
                print("! {}".format(am.person.email), file=fd)
