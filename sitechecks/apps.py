from django.apps import AppConfig


class SitechecksConfig(AppConfig):
    name = 'sitechecks'
