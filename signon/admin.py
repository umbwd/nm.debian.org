from django.contrib import admin
from .models import Identity


@admin.register(Identity)
class IdentityAdmin(admin.ModelAdmin):
    search_fields = (
        "username",
        "person__fullname", "person__email",
    )

    def save_model(self, request, obj, form, change):
        """
        Given a model instance save it to the database.
        """
        obj.save(audit_author=request.user, audit_notes="edited from admin")
