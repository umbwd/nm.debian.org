from django.apps import AppConfig


class SignonConfig(AppConfig):
    name = 'signon'
