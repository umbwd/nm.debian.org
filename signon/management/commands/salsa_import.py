from __future__ import annotations
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.conf import settings
from backend.models import Person
from signon.models import Identity
import logging
import json
import gitlab

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "import Salsa user information"

    def add_arguments(self, parser):
        parser.add_argument('--dry-run', action="store_true",
                            help="log what would happen and perform no changes")
        parser.add_argument('--crosscheck', metavar="file.json", action="store",
                            help="cross check data from a {username: subject} mapping")

    def handle(self, *args, crosscheck=None, dry_run=False, **opts):
        salsa_host = getattr(settings, "SALSA_HOST", "https://salsa.debian.org")

        token = getattr(settings, "SALSA_TOKEN", None)
        if token is None:
            raise CommandError("SALSA_TOKEN must be defined in settings")

        if crosscheck:
            with open(crosscheck, "rt") as fd:
                valid_ids = json.load(fd)
        else:
            valid_ids = None

        # Load Salsa username -> id mapping
        by_name = {}
        gl = gitlab.Gitlab(salsa_host, private_token=token)
        for user in gl.users.list(all=True):
            by_name[user.username] = {
                "id": user.id,
                "profile": user.web_url,
                "picture": user.avatar_url,
                "fullname": user.name,
                "username": user.username,
            }
        log.info("%d users found by salsa API", len(by_name))

        # Load existing salsa identity information
        by_person = {}
        for identity in Identity.objects.filter(issuer="salsa"):
            by_person[identity.person_id] = identity

        housekeeper = Person.objects.get_housekeeper()

        with transaction.atomic():
            for person in Person.objects.select_related("ldap_fields").all():
                # Lookup the salsa ID for this person
                salsa_info = by_name.get(person.ldap_fields.uid)
                if salsa_info is None:
                    continue

                salsa_id = salsa_info["id"]

                if valid_ids is not None:
                    crosscheck_id = valid_ids.pop(person.ldap_fields.uid, None)
                    if crosscheck_id is None:
                        log.error("%s: has ldap UID %s but is not in crosscheck export: skipped",
                                  person, person.ldap_fields.uid)
                        continue
                    elif crosscheck_id != str(salsa_id):
                        log.error("%s: has Salsa ID %s from the API and %s in crosscheck export: skipped",
                                  person, salsa_id, crosscheck_id)
                        continue

                # Lookup an existing Identity entry for this person
                identity = by_person.pop(person.id, None)
                if identity is None:
                    log.info("Adding salsa ID for %s: %d",
                             person.lookup_key, salsa_id)
                    if not dry_run:
                        identity = Identity.objects.create(
                            person=person, issuer="salsa", subject=str(salsa_id),
                            audit_author=housekeeper,
                            audit_notes="Created on salsa import",
                        )
                elif identity.subject != str(salsa_id):
                    log.error("Salsa ID for %s changed from %s to %d",
                              person.lookup_key, identity.subject, salsa_id)
                    if not dry_run:
                        identity.subject = str(salsa_id)
                        identity.save(
                            audit_author=housekeeper,
                            audit_notes="Subject updated on salsa import",
                        )

                if not dry_run:
                    identity.update(
                        profile=salsa_info["profile"],
                        picture=salsa_info["picture"],
                        fullname=salsa_info["fullname"],
                        username=salsa_info["username"],
                        audit_author=housekeeper,
                        audit_notes="Imported user information from salsa",
                    )

            # Detect subjects that disappeared on Salsa
            for identity in by_person.values():
                log.error("Salsa ID for %s was %s and now disappeared",
                          identity.person.lookup_key, identity.subject)
                if not dry_run:
                    identity.delete()

        if valid_ids is not None:
            for username in valid_ids.keys():
                if username not in by_name:
                    log.warning("%s: user found in crosscheck export and not in salsa API", username)
                else:
                    log.warning("%s: user found in crosscheck export and not in nm.debian.org", username)
