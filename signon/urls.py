from django.urls import path
from . import views

app_name = "signon"

urlpatterns = [
    path('logout/', views.Logout.as_view(), name='logout'),
    path('oidc/callback/<name>/', views.OIDCAuthenticationCallbackView.as_view(), name='oidc_callback'),
    path('whoami/', views.Whoami.as_view(), name='whoami'),
]
