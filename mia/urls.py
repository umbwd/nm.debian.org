from __future__ import annotations
from django.urls import path
from . import views

app_name = "mia"

urlpatterns = [
    path('uploaders/', views.Uploaders.as_view(), name="uploaders"),
    path('voters/', views.Voters.as_view(), name="voters"),
    path('wat/ping/<key>/', views.MIAPing.as_view(), name="wat_ping"),
    path('wat/remove/<int:pk>/', views.MIARemove.as_view(), name="wat_remove"),
    path('fd/', views.FrontDesk.as_view(), name="fd"),
]
