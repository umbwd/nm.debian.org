from __future__ import annotations
from typing import Optional
from dataclasses import dataclass
from nm2.lib import assets


@dataclass
class NavLink:
    url: str
    label: str
    icon: Optional[str] = None


class NM2LayoutMixin(assets.AssetMixin):
    assets = [assets.Bootstrap4, assets.ForkAwesome]

    def get_person_menu_entries(self):
        return []

    def get_process_menu_entries(self):
        return []

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["navbar"] = {
            "person": self.get_person_menu_entries(),
            "process": self.get_process_menu_entries(),
        }
        return ctx
